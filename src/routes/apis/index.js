import express from 'express'
import Middleware from '../../middlewares'
import HistoryValidator from '../../validators/HistoryValidator'
import HistoryController from '../../controllers/HistoryController'
import BiodataValidator from '../../validators/BiodataValidator'
import BiodataController from '../../controllers/BiodataController'
import UserValidator from '../../validators/UserValidator'
import UserController from '../../controllers/UserController'

const router = express.Router()

router.get('/user', UserController.get)
router.post('/user',UserController.create)
router.patch('/user/:id', UserController.update)
router.delete('/user/:id', UserController.delete)

router.get('/biodata', BiodataController.get)
router.post('/biodata', BiodataController.create)
router.patch('/biodata/:id', BiodataController.update)
router.delete('/biodata/:id', BiodataController.delete)

router.get('/history', HistoryController.get)
router.post('/history', HistoryController.create)
router.patch('/history/:id', HistoryController.update)
router.delete('/history/:id', HistoryController.delete)

export default router