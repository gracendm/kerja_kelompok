import { body, param } from 'express-validator'

class BiodataValidator {
  static create = () => [
    body('name').exists().isString(),
    body('gender').optional().isString(),
  ]

  static update = () => [
    param('id').exists().isUUID(),
    body('name').optional().isString(),
    body('gender').optional().isString(),
  ]

  static delete = () => [
    param('id').exists().isUUID(),
  ]
}

export default BiodataValidator
