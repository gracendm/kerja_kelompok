module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Biodata', [
      {
        id: 'a9dea600-0437-4956-895d-62864f039227',
        name: 'alpha',
        gender: 'm',
        userId: '71b6e88f-3adb-433b-9566-ed5097438742',
        createdAt: new Date(),
        updatedAt: new Date()
      },{
        id: '94697ce9-8629-48ec-a66c-a06f92eea8ec',
        name: 'beta',
        gender: 'm',
        userId: '115c9c91-5d7a-4324-a1be-e47d681f9757',
        createdAt: new Date(),
        updatedAt: new Date()
      },{
        id: 'e8201a69-82d2-4ce0-889b-9c387eafaac4',
        name: 'hgdwhgug',
        gender: 'f',
        userId: '3074c4b8-63db-4297-8189-57bfa1beac96',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
    
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Biodata', null, {});
  }
};
