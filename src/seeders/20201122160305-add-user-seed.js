module.exports = {
  up: async (queryInterface, Sequelize) => {
  
     await queryInterface.bulkInsert('User', [
       {
          id: '71b6e88f-3adb-433b-9566-ed5097438742',
          user_name: 'putri',
          email: 'martin@gmail.com',
          password: 'putri',
          createdAt: new Date(),
          updatedAt: new Date() 
        }, {
          id: '115c9c91-5d7a-4324-a1be-e47d681f9757',
          user_name: 'iswandari',
          email: 'joseph@gmail.com',
          password: 'joseph',
          createdAt: new Date(),
          updatedAt: new Date()
        },{
          id: '3074c4b8-63db-4297-8189-57bfa1beac96',
          user_name: 'damayanti',
          email:'xyzq@gmail.com',
          password: 'xyzq',
          createdAt: new Date(),
          updatedAt: new Date()
      }], {});
    
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('User', null, {});
     
  }
};
