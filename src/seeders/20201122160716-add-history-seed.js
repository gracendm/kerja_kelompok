module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('History', [
      {
        id: 'f5110273-abb3-4c77-9828-031b96f61df6',
        score : 'Lose',
        time: '2002-10-09',
        userId: '71b6e88f-3adb-433b-9566-ed5097438742',
        createdAt: new Date(),
        updatedAt: new Date()
      },{
        id: 'c5026f4d-2459-4b94-9b09-ada4e924989c',
        score: 'Lose',
        time: '2020-11-19',
        userId: '115c9c91-5d7a-4324-a1be-e47d681f9757',
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        id: '0dcef496-44fc-4d18-92ad-ef08a5c68d96',
        score: 'Draw',
        time: '2010-11-15',
        userId: '3074c4b8-63db-4297-8189-57bfa1beac96',
        createdAt: new Date(),
        updatedAt: new Date()
      }], {});
    
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('History', null, {});
     
  }
};
