import { User } from '../../models'

class UserController {
  static get = (req, res) => User.findAll().then(
    (user) => res.status(200).json(user),
  ).catch(
    (e) => console.log(e),
  )

  static create = (req, res) => {
    const { email, password, user_name } = req.body

    return User.create({
      email,
      password,
      user_name
    }).then(
      (data) => res.status(201).json({ ...data.dataValues }),
    ).catch(
      (e) => console.log(e),
    )
  }

  static update = (req, res) => {
    const { id } = req.params

    return User.findOne({
      where: { id },
    }).then(
      (user) => {
        if (!user) return res.status(404).json({ message: 'Not found' })

        const { email, password, user_name } = req.body

        return user.update(
          { email, password, user_name },
        ).then(
          (updated) => res.status(200).json({ ...updated.dataValues }),
        )
      },
    ).catch(
      (e) => console.log(e),
    )
  }

  static delete = (req, res) => {
    const { id } = req.params

    return User.destroy({
      where: { id },
    }).then(
      (user) => {
        if (!user) return res.status(404).json({ message: 'Not found' })

        return res.status(200).json({ message: 'Deleted' })
      },
    ).catch(
      (e) => console.log(e),
    )
  }
}

export default UserController