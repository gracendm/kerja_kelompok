import { History } from '../../models'

class HistoryController {
  static get = (req, res) => History.findAll().then(
    (history) => res.status(200).json(history),
  ).catch(
    (e) => console.log(e),
  )

  static create = (req, res) => {
    const {score, time } = req.body

    return History.create({
      score,
      time
    }).then(
      (data) => res.status(201).json({ ...data.dataValues }),
    ).catch(
      (e) => console.log(e),
    )
  }

  static update = (req, res) => {
    const { id } = req.params

    return History.findOne({
      where: { id },
    }).then(
      (history) => {
        if (!history) return res.status(404).json({ message: 'Not found' })

        const { score, time } = req.body

        return history.update(
          { score, time },
        ).then(
          (updated) => res.status(200).json({ ...updated.dataValues }),
        )
      },
    ).catch(
      (e) => console.log(e),
    )
  }

  static delete = (req, res) => {
    const { id } = req.params

    return History.destroy({
      where: { id },
    }).then(
      (history) => {
        if (!history) return res.status(404).json({ message: 'Not found' })

        return res.status(200).json({ message: 'Deleted' })
      },
    ).catch(
      (e) => console.log(e),
    )
  }
}

export default HistoryController