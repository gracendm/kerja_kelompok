import { Biodata } from '../../models'

class BiodataController {
  static get = (req, res) => Biodata.findAll().then(
    (biodata) => res.status(200).json(biodata),
  ).catch(
    (e) => console.log(e),
  )

  static create = (req, res) => {
    const { name, gender, } = req.body

    return Biodata.create({
      name,
      gender,
    }).then(
      (data) => res.status(201).json({ ...data.dataValues }),
    ).catch(
      (e) => console.log(e),
    )
  }

  static update = (req, res) => {
    const { id } = req.params

    return Biodata.findOne({
      where: { id },
    }).then(
      (biodata) => {
        if (!biodata) return res.status(404).json({ message: 'Not found' })

        const { name, gender} = req.body

        return biodata.update(
          { name, gender},
        ).then(
          (updated) => res.status(200).json({ ...updated.dataValues }),
        )
      },
    ).catch(
      (e) => console.log(e),
    )
  }

  static delete = (req, res) => {
    const { id } = req.params

    return Biodata.destroy({
      where: { id },
    }).then(
      (biodata) => {
        if (!biodata) return res.status(404).json({ message: 'Not found' })

        return res.status(200).json({ message: 'Deleted' })
      },
    ).catch(
      (e) => console.log(e),
    )
  }
}

export default BiodataController