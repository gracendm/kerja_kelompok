import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    static associate(models) {

      const { Biodata, History } = models

      User.hasOne(Biodata, { foreignKey: 'id' })
      User.hasMany(History, { foreignKey: 'id' })
    }
  };
  User.init( {
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    user_name: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
    timestamps: true
  });
  return User;
};

