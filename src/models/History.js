import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class History extends Model {
    static associate(models) {
      const { User } = models

      History.belongsTo(User, { foreignKey: 'userId', as: 'userInfo' })
    }
  };
  History.init({
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    score: DataTypes.STRING,
    time: DataTypes.DATE,
    userId: DataTypes.UUID
  }, {
    sequelize,
    modelName: 'History',
    timestamps: true
  });
  return History;
};


