import { validationResult } from 'express-validator'
import AuthMiddleware from './Auth'

class Middleware {

  static Auth = (req, res, next) => this.handler('auth', req, res, next)

  static User = () => {}

  static handler = (type, req, res, next) => {
    const errors = validationResult(req)

    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() })
    }

    switch (type) {
      case 'auth': return AuthMiddleware(req, res, next)
      default: return res.status(403)
    }
  }
}

export default Middleware