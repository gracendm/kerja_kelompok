const BasicAuth = (req, res, next) => {
    const { headers: { authorization } } = req
    // Basic hasgtdiusadhss=  ==> ['Basic', 'hasgtdiusadhss=']
  
    const b64auth = (authorization || '').split(' ')[1] || ''
    const [username, password] = Buffer.from(b64auth, 'base64').toString().split(':')
    // username:password => ['username', 'password']
  
    // 123 => bcrypt => jsadosa798jkasjd98a
  
    console.log(username, password)
    // TODO: Validate User
  
    return next()
  }
  
  const AuthMiddleware = (req, res, next) => {
    BasicAuth(req, res, next)
  }
  
  export default AuthMiddleware
  